<?php
IncludeModuleLangFile(__FILE__);

class mcart_antiplagiarism extends CModule
{
	var $MODULE_ID = "mcart.antiplagiarism";
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	//var $MODULE_GROUP_RIGHTS = "Y";
    var $PARTNER_NAME;
    var $PARTNER_URI;

	function mcart_antiplagiarism()
	{
        $this->PARTNER_NAME = GetMessage('PARTNER_NAME_ANTIPLAG');
        $this->PARTNER_URI = "http://mcart.ru/";
		$arModuleVersion = array();

		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");

		if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
		{
			$this->MODULE_VERSION = $arModuleVersion["VERSION"];
			$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		}

		$this->MODULE_NAME = GetMessage("MODULE_NAME_ANTIPLAG");
		$this->MODULE_DESCRIPTION = GetMessage("MODULE_DESCRIPTION_ANTIPLAG");
	}

	function DoInstall()
	{
		$this->InstallFiles();
		$this->InstallDB();
	}

	function InstallDB()
	{
        if(!CModule::IncludeModule("iblock"))
            die(GetMessage("IBLOCK_ERROR"));
        if(!class_exists('SoapClient'))
            die(GetMessage("IBLOCK_SOAP"));

        RegisterModule("mcart.antiplagiarism");
        global $DB;

        //create IB type
        $iblockTypeID = false;
        $dbIblockType=CIBlockType::GetList(array(),array("ID"=>"mcartantiplogserv"));
        if($arIblockType=$dbIblockType->Fetch())
        {
            $iblockTypeID = $arIblockType["ID"];
        }
        else
        {
            $arFields = Array(
                'ID'=>'mcartantiplogserv',
                'SECTIONS'=>'Y',
                'IN_RSS'=>'N',
                'SORT'=>1000,
                'LANG'=>Array(
                    'en'=>Array(
                        'NAME'=>'Antiplagiarism',
                        'SECTION_NAME'=>'Sections',
                        'ELEMENT_NAME'=>'Products'
                    ),
                    'ru'=>Array(
                        'NAME'=>GetMessage("ANTIPLOG_NAME_IBLOCK_TYPE"),
                        'SECTION_NAME'=>GetMessage("ANTIPLOG_SECTIONS_NAME"),
                        'ELEMENT_NAME'=>GetMessage("ANTIPLOG_ELEMENTS_NAME")
                    )
                )
            );


            global $DB;
            $obBlocktype = new CIBlockType;
            $DB->StartTransaction();
            $iblockTypeID = $obBlocktype->Add($arFields);
            if(!$iblockTypeID)
            {
                $DB->Rollback();
                echo 'Error: '.$obBlocktype->LAST_ERROR.'<br>';
            }
            else
                $DB->Commit();
        }

        //create IB

        if($iblockTypeID !== false)
        {
            //iblockID
            $iblockID=false;
            $dbIblock=CIBlock::GetList(array(),array("TYPE"=>"mcartantiplogserv"));
            if($arIblock=$dbIblock->Fetch())
            {
                $iblockID=$arIblock["ID"];
            }
            else
            {
                global $DB;
                $ib = new CIBlock;
                $arFields = Array(
                    "ACTIVE" => "Y",
                    "NAME" => GetMessage("ANTIPLAG_IBLOCK_NAME"),
                    "CODE" => "turn_in_antiplagiarism",
                    "IBLOCK_TYPE_ID" => $iblockTypeID,
                    "SITE_ID" => Array("s1"),
                    "SORT" => 100,
                    "GROUP_ID" => Array("2"=>"D"),
                    "WF_TYPE"=>"N"
                );

                $iblockID = $ib->Add($arFields);
                if(!$iblockID)
                {
                    $DB->Rollback();
                    echo 'Error: '.$ib->LAST_ERROR.'<br>';
                }
                else
                    $DB->Commit();
            }
        }

        //create prop IB
        if($iblockID) {

            $arFields = Array(
                "NAME" => GetMessage("ID_IBL_LIST"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "ID_IBL_LIST",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

            $arFields = Array(
                "NAME" => GetMessage("ID_FILE_FROM_LIST"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "ID_FILE_FROM_LIST",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

            $arFields = Array(
                "NAME" => GetMessage("CHACK_STATUS"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "CHACK_STATUS",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

            $arFields = Array(
                "NAME" => GetMessage("ID_FILE_FROM_SERVER"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "ID_FILE_FROM_SERVER",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

            $arFields = Array(
                "NAME" => GetMessage("PATH_FILE_FROM_SERVER"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "PATH_FILE_FROM_SERVER",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

            $arFields = Array(
                "NAME" => GetMessage("ID_IN_ANTIPLOG"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "ID_IN_ANTIPLOG",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

            $arFields = Array(
                "NAME" => GetMessage("PROC_PLAG"),
                "ACTIVE" => "Y",
                "SORT" => "500",
                "MULTIPLE" => "N",
                "CODE" => "PROC_PLAG",
                "PROPERTY_TYPE" => "S",
                "IBLOCK_ID" => $iblockID,
            );

            $ibp = new CIBlockProperty;
            $PropStringID = $ibp->Add($arFields);

        }
        //create agents

        $stmp = MakeTimeStamp(date("d.m.Y h:i:00"), CSite::GetDateFormat());
        $arrAdd = array("MI"=> 3);
        $stmp = AddToTimeStamp($arrAdd, $stmp);


        CAgent::AddAgent(
            "CAgentsAntyplag::send_file_to_antiplag();",
            "mcart.antiplagiarism",
            "Y",
            60,
            "",
            "Y",
            date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")),$stmp),
            100
        );

        CAgent::AddAgent(
            "CAgentsAntyplag::chack_status_file_antiplag();",
            "mcart.antiplagiarism",
            "Y",
            60,
            "",
            "Y",
            date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")),$stmp),
            100
        );


		//$eventManager = \Bitrix\Main\EventManager::getInstance();

		//$eventManager->registerEventHandler('voximplant', 'OnCallEnd', 'mcart.misscalls', 'CCallEndEv', 'onCallEndHandler');

		return true;
	}

	function InstallFiles()
	{
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/mcart.antiplagiarism/install/activities/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/activities", true, true);
		return true;
	}

	function InstallEvents()
	{
		return true;
	}

	function DoUninstall()
	{

			$this->UnInstallDB();
			$this->UnInstallFiles();
	}

	function UnInstallDB()
	{
		//$eventManager = \Bitrix\Main\EventManager::getInstance();
		//$eventManager->unRegisterEventHandler('voximplant', 'OnCallEnd', 'mcart.misscalls', 'CCallEndEv', 'onCallEndHandler');
		UnRegisterModule("mcart.antiplagiarism");

		return true;
	}

	function UnInstallFiles()
	{
        DeleteDirFilesEx("/bitrix/activities/custom/queueactivity/");
        DeleteDirFilesEx("/bitrix/activities/custom/returncheckfileactivity/");
		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}
}
