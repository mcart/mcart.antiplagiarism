<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use \Bitrix\Rest\Sqs;

class CBPReturnCheckFileActivity extends CBPActivity
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->arProperties = array(
            //"Title" => "",
            "Docc" => "",
            "ReturnValuee" => ""
        );
    }

    public function Execute()
    {
        //if(!static::checkRegister())
        //{
        //    return CBPActivityExecutionStatus::Closed;
        //}

        $doc = $this->Docc;
        $doc = intval($doc);

        if(!CModule::IncludeModule("iblock"))
            return false;
        //$this->ReturnValue = -1;
        //while($this->ReturnValue == -1) {
            //sleep(10);
            $res = CIBlock::GetList(Array(), Array("CODE" => 'turn_in_antiplagiarism'), true);
            while ($ar_res = $res->Fetch()) {
                $idIBlockQ = $ar_res['ID'];
            }

            //echo $idIBlockQ;

            $arFilter = array(
                "IBLOCK_ID" => $idIBlockQ,
                "ID" => $doc,
                "PROPERTY_CHACK_STATUS" => "Ready"
            );
            $arSelect = Array("PROPERTY_PROC_PLAG", "PROPERTY_CHACK_STATUS");

            $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize" => 50), $arSelect);

            while ($ob = $res->Fetch()) {
                $arFile = $ob;
            }
            $this->ReturnValuee = -1;
            if (!empty($arFile)) {
                $this->ReturnValuee = $arFile['PROPERTY_PROC_PLAG_VALUE'];
            }

            //$this -> ReturValue =
        //}

        return CBPActivityExecutionStatus::Closed;
    }

    public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "", $popupWindow = null, $siteId = '')
    {
        $dialog = new \Bitrix\Bizproc\Activity\PropertiesDialog(__FILE__, array(
            'documentType' => $documentType,
            'activityName' => $activityName,
            'workflowTemplate' => $arWorkflowTemplate,
            'workflowParameters' => $arWorkflowParameters,
            'workflowVariables' => $arWorkflowVariables,
            'currentValues' => $arCurrentValues,
            'formName' => $formName,
            'siteId' => $siteId
        ));

        $dialog->setMap(array(
            'Docc' => array(
                'Name' => GetMessage('ID_ELEM_WITH_FILE_NAME'),
                'FieldName' => 'docc',
                'Type' => 'string',
                'Required' => true
            )
        ));

        return $dialog;
    }

    public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
    {
        $arErrors = array();

        $arProperties = array(
            "Docc" => $arCurrentValues["docc"]
        );

        $arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
        if (count($arErrors) > 0)
            return false;

        $arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
        $arCurrentActivity["Properties"] = $arProperties;

        return true;
    }

}
