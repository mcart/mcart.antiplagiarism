<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<tr>
    <td align="right" width="40%" valign="top"><span class="adm-required-field"><?= GetMessage("ID_ELEM_WITH_FILE") ?>:</span></td>
    <td width="60%">
        <?=CBPDocument::ShowParameterField("string", 'docc', $arCurrentValues['docc'])?>
    </td>
</tr>