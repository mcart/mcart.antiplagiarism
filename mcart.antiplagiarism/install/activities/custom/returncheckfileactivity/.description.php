<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
    "NAME" => GetMessage("RCF_DESCR_NAME"),
    "DESCRIPTION" => GetMessage("RCF_DESCR_DESCR"),
    "TYPE" => array('activity', 'robot_activity'),  // ��� - ��������
    "CLASS" => "ReturnCheckFileActivity", //����� � Activity
    "JSCLASS" => "BizProcActivity",  //����������� JS ����������, ������� ����� ������� Activity
    "CATEGORY" => array(
        "ID" => "other", // Activity ����� ������������� � ��������� "������"
    ),
    "RETURN" => array(
            "ReturnValuee" => array(
            "NAME" => GetMessage("RCF_RET_VALUE"),
            "TYPE" => "string",
        ),
    ),
);