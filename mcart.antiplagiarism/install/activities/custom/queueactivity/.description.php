<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
    "NAME" => GetMessage("QUE_DESCR_NAME"),
    "DESCRIPTION" => GetMessage("QUE_DESCR_DESCR"),
    "TYPE" => array('activity', 'robot_activity'),  // ��� - ��������
    "CLASS" => "QueueActivity", //����� � Activity
    "JSCLASS" => "BizProcActivity",  //����������� JS ����������, ������� ����� ������� Activity
    "CATEGORY" => array(
        "ID" => "other", // Activity ����� ������������� � ��������� "������"
    ),
    "RETURN" => array(
        "ReturnValue" => array(
            "NAME" => GetMessage("QUE_RET_VALUE"),
            "TYPE" => "string",
        ),
        "ReturnValueEr" => array(
            "NAME" => GetMessage("QUE_RET_VALUE_ER"),
            "TYPE" => "string",
        ),
    ),
);