<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<tr>
    <td align="right" width="40%" valign="top"><span class="adm-required-field"><?= GetMessage("ID_IBLOCK_WITH_FILE") ?>:</span></td>
    <td width="60%">
        <?=CBPDocument::ShowParameterField("string", 'iblock', $arCurrentValues['iblock'])?>
    </td>
</tr>
<tr>
    <td align="right" width="40%" valign="top"><span class="adm-required-field"><?= GetMessage("ID_FILE_IN_LIST") ?>:</span></td>
    <td width="60%">
        <?=CBPDocument::ShowParameterField("string", 'doc', $arCurrentValues['doc'])?>
    </td>
</tr>
<tr>
    <td align="right" width="40%" valign="top"><span class="adm-required-field"><?= GetMessage("CODE_FIELD_FILE") ?>:</span></td>
    <td width="60%">
        <?=CBPDocument::ShowParameterField("string", 'codefile', $arCurrentValues['codefile'])?>
    </td>
</tr>