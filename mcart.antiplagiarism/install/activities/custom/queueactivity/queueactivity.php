<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use \Bitrix\Rest\Sqs;

class CBPQueueActivity extends CBPActivity
{
    public function __construct($name)
    {
        parent::__construct($name);
        $this->arProperties = array(
            //"Title" => "",
            "Doc" => "",
            "IBlockID" => "",
            "CodeFile" => "",
            "ReturnValue" => "",
            "ReturnValueEr" => ""
        );
    }

    public function Execute()
    {
        //if(!static::checkRegister())
        //{
        //    return CBPActivityExecutionStatus::Closed;
        //}

        $iblock = $this->IBlockID;
        $iblock = intval($iblock);
        $doc = $this->Doc;
        $doc = intval($doc);
        $codefilef = $this->CodeFile;
        //$codefilef = trim($codefilef);

        if(!CModule::IncludeModule("iblock"))
            return false;

        $res = CIBlock::GetList(Array(), Array("CODE" => 'turn_in_antiplagiarism'), true);
        while($ar_res = $res->Fetch())
        {
            $idIBlockQ = $ar_res['ID'];
        }

        echo $idIBlockQ;

        $arFilter = array(
            "IBLOCK_ID" => $iblock,
            "ID" => $doc,
        );
        $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_".$codefilef);

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
        $PROP = array();
        while($ob = $res->Fetch())
        {
            $PROP["ID_IBL_LIST"] = $ob['IBLOCK_ID'];
            $PROP["ID_FILE_FROM_LIST"] = $ob['ID'];
            $PROP["CHACK_STATUS"] = "New";
            $PROP["ID_FILE_FROM_SERVER"] = $ob['PROPERTY_'.$codefilef.'_VALUE'];
            $PROP["PATH_FILE_FROM_SERVER"] = CFile::GetPath($ob["PROPERTY_".$codefilef."_VALUE"]);
        }

        //echo "<pre>";
        //print_r($PROP);
        //echo "</pre>";
        if(!empty($PROP["PATH_FILE_FROM_SERVER"])) {
            $path_parts = pathinfo($PROP["PATH_FILE_FROM_SERVER"]);
            if ($path_parts['extension'] == 'doc' || $path_parts['extension'] == 'docx') {
                $el = new CIBlockElement;

                global $USER;

                $arLoadProductArray = Array(
                    "MODIFIED_BY" => $USER->GetID(), // ������� ������� ������� �������������
                    "IBLOCK_SECTION_ID" => false,          // ������� ����� � ����� �������
                    "IBLOCK_ID" => $idIBlockQ,
                    "PROPERTY_VALUES" => $PROP,
                    "NAME" => "file" . $PROP["ID_FILE_FROM_LIST"],
                    "ACTIVE" => "Y",            // �������
                    //"PREVIEW_TEXT"   => "����� ��� ������ ���������",
                    //"DETAIL_TEXT"    => "����� ��� ���������� ���������",
                    //"DETAIL_PICTURE" => CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/image.gif")
                );
                if ($PRODUCT_ID = $el->Add($arLoadProductArray)) {
                    $this->ReturnValue = $PRODUCT_ID;
                    $this->ReturnValueEr = 0;
                } else {
                    $this->ReturnValue = 0;
                    $this->ReturnValueEr = GetMessage('ANTIPLAG_ERROR');
                    //$this -> ReturValue = "sdfsdfsdf";
                }
            } else {
                $this->ReturnValue = 0;
                $this->ReturnValueEr = GetMessage('ANTIPLAG_ERROR_EX');
            }
        } else {
            $this->ReturnValue = 0;
            $this->ReturnValueEr = GetMessage('ANTIPLAG_ERROR_EMPTY_F');
        }
        return CBPActivityExecutionStatus::Closed;
    }

    public static function GetPropertiesDialog($documentType, $activityName, $arWorkflowTemplate, $arWorkflowParameters, $arWorkflowVariables, $arCurrentValues = null, $formName = "", $popupWindow = null, $siteId = '')
    {
        $dialog = new \Bitrix\Bizproc\Activity\PropertiesDialog(__FILE__, array(
            'documentType' => $documentType,
            'activityName' => $activityName,
            'workflowTemplate' => $arWorkflowTemplate,
            'workflowParameters' => $arWorkflowParameters,
            'workflowVariables' => $arWorkflowVariables,
            'currentValues' => $arCurrentValues,
            'formName' => $formName,
            'siteId' => $siteId
        ));

        $dialog->setMap(array(
            'Doc' => array(
                'Name' => GetMessage('ID_IBLOCK_WITH_FILE_NAME'),
                'FieldName' => 'doc',
                'Type' => 'string',
                'Required' => true
            ),
            'IBlockID' => array(
                'Name' => GetMessage('ID_FILE_IN_LIST_NAME'),
                'FieldName' => 'iblock',
                'Type' => 'string',
                'Required' => true
            ),
            'CodeFile' => array(
                'Name' => GetMessage('CODE_FIELD_FILE_NAME'),
                'FieldName' => 'codefile',
                'Type' => 'string',
                'Required' => true
            )
        ));

        return $dialog;
    }

    public static function GetPropertiesDialogValues($documentType, $activityName, &$arWorkflowTemplate, &$arWorkflowParameters, &$arWorkflowVariables, $arCurrentValues, &$arErrors)
    {
        $arErrors = array();

        $arProperties = array(
            "IBlockID" => $arCurrentValues["iblock"],
            "Doc" => $arCurrentValues["doc"],
            "CodeFile" => $arCurrentValues["codefile"]
        );

        $arErrors = self::ValidateProperties($arProperties, new CBPWorkflowTemplateUser(CBPWorkflowTemplateUser::CurrentUser));
        if (count($arErrors) > 0)
            return false;

        $arCurrentActivity = &CBPWorkflowTemplateLoader::FindActivityByName($arWorkflowTemplate, $activityName);
        $arCurrentActivity["Properties"] = $arProperties;

        return true;
    }

}
