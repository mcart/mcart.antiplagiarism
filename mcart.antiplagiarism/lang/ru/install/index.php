<?
$MESS ['MODULE_NAME_ANTIPLAG'] = "Связь с внешним сервисом антиплагиат";
$MESS ['MODULE_DESCRIPTION_ANTIPLAG'] = "Связь с внешним сервисом антиплагиат";
$MESS ['PARTNER_NAME_ANTIPLAG'] = "Эм Си Арт";

$MESS ['ANTIPLOG_NAME_IBLOCK_TYPE'] = "Антиплагиат";
$MESS ['ANTIPLOG_SECTIONS_NAME'] = "Разделы";
$MESS ['ANTIPLOG_ELEMENTS_NAME'] = "Элементы";

$MESS ['IBLOCK_ERROR'] = "Модуль Информационных блоков не установлен";
$MESS ['IBLOCK_SOAP'] = "Класс SoapClient не найден";
$MESS ['ANTIPLAG_IBLOCK_NAME'] = "Очередь в антиплагиат";

$MESS ['ID_IBL_LIST'] = "ID инфоблока с файлом";
$MESS ['ID_FILE_FROM_LIST'] = "ID файла в списке";
$MESS ['CHACK_STATUS'] = "Статус проверки ";
$MESS ['ID_FILE_FROM_SERVER'] = "ID файла на сервере";
$MESS ['PATH_FILE_FROM_SERVER'] = "Путь к файлу на сервере";
$MESS ['ID_IN_ANTIPLOG'] = "ID файла в антиплагиате";
$MESS ['PROC_PLAG'] = "Процент плагиата";
?>