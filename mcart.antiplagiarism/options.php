<?

global $MESS;
IncludeModuleLangFile(__FILE__);

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");

$module_id = "mcart.antiplagiarism";
CModule::IncludeModule($module_id);

$ADDRESS_CUR_SITE = COption::GetOptionString("mcart.antiplagiarism", "ADDRESS_CUR_SITE", "");
$ADDRESS_ANTIPLAG = COption::GetOptionString("mcart.antiplagiarism", "ADDRESS_ANTIPLAG", "http://testapi.antiplagiat.ru");
$ANTIPLAG_LOGIN = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_LOGIN", "testapi@antiplagiat.ru");
$ANTIPLAG_PASS = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_PASS", "testapi");
$ANTIPLAG_COMPANY_NAME = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_COMPANY_NAME", "testapi");
$ANTIPLAG_APICORP_ADDRESS = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_APICORP_ADDRESS", "api.antiplagiat.ru:44902");
$Q_FILES_ALL = COption::GetOptionString("mcart.antiplagiarism", "Q_FILES_ALL", "10");
$SERVICES_FOR_CHECK = COption::GetOptionString("mcart.antiplagiarism", "SERVICES_FOR_CHECK", "wikipedia");

$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);
if ($MOD_RIGHT >= "Y" || $USER->IsAdmin()):
    if (!empty($_POST)) {
        if($ADDRESS_CUR_SITE = $_POST["ADDRESS_CUR_SITE"]) {
            COption::SetOptionString("mcart.antiplagiarism", "ADDRESS_CUR_SITE", $ADDRESS_CUR_SITE);
        }
        if($ADDRESS_ANTIPLAG = $_POST["ADDRESS_ANTIPLAG"]) {
            COption::SetOptionString("mcart.antiplagiarism", "ADDRESS_ANTIPLAG", $ADDRESS_ANTIPLAG);
        }
        if($ANTIPLAG_LOGIN = $_POST["ANTIPLAG_LOGIN"]) {
            COption::SetOptionString("mcart.antiplagiarism", "ANTIPLAG_LOGIN", $ANTIPLAG_LOGIN);
        }
        if($ANTIPLAG_PASS = $_POST["ANTIPLAG_PASS"]) {
            COption::SetOptionString("mcart.antiplagiarism", "ANTIPLAG_PASS", $ANTIPLAG_PASS);
        }
        if($ANTIPLAG_COMPANY_NAME = $_POST["ANTIPLAG_COMPANY_NAME"]) {
            COption::SetOptionString("mcart.antiplagiarism", "ANTIPLAG_COMPANY_NAME", $ANTIPLAG_COMPANY_NAME);
        }
        if($ANTIPLAG_APICORP_ADDRESS = $_POST["ANTIPLAG_APICORP_ADDRESS"]) {
            COption::SetOptionString("mcart.antiplagiarism", "ANTIPLAG_APICORP_ADDRESS", $ANTIPLAG_APICORP_ADDRESS);
        }
        if($Q_FILES_ALL = $_POST["Q_FILES_ALL"]) {
            COption::SetOptionString("mcart.antiplagiarism", "Q_FILES_ALL", $Q_FILES_ALL);
        }
        if($SERVICES_FOR_CHECK = $_POST["SERVICES_FOR_CHECK"]) {
            COption::SetOptionString("mcart.antiplagiarism", "SERVICES_FOR_CHECK", $SERVICES_FOR_CHECK);
        }

    }
        $aTabs = array(
            array("DIV" => "edit1", "TAB" => GetMessage('ANTIPLAG_OP'), "ICON" => "main_settings", "TITLE" => GetMessage('ANTIPLAG_OP')),
        );

        $tabControl = new CAdminTabControl("tabControl", $aTabs);
        ?>
        <?
        $tabControl->Begin();
        ?>


            <style>
                #tblTYPES tr td 			{vertical-align: top;}
                #tblTYPES .wd-quick-edit 	{display: none; width: 500px;}
                #tblTYPES .wd-quick-view	{padding: 3px; border: 1px solid transparent; width:800px;}
                #tblTYPES .wd-input-hover 	{background-color:#F8F8F8; border: 1px solid #bbbbbb; cursor: pointer;}
                textarea { word-wrap: break-word; }
            </style>

        <form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialchars($mid) ?>&lang=<?= LANGUAGE_ID ?>" name="antiplag_settings">
        <? $tabControl->BeginNextTab(); ?>
            <tr>
                <td ><? echo GetMessage('ADDRESS_CUR_SITE') ?></td>
                <td><input type="text" name="ADDRESS_CUR_SITE" id="ADDRESS_CUR_SITE" value=<?=$ADDRESS_CUR_SITE?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('ADDRESS_ANTIPLAG') ?></td>
                <td><input type="text" name="ADDRESS_ANTIPLAG" id="ADDRESS_ANTIPLAG" value=<?=$ADDRESS_ANTIPLAG?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('ANTIPLAG_LOGIN') ?></td>
                <td><input type="text" name="ANTIPLAG_LOGIN" id="ANTIPLAG_LOGIN" value=<?=$ANTIPLAG_LOGIN?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('ANTIPLAG_PASS') ?></td>
                <td><input type="text" name="ANTIPLAG_PASS" id="ANTIPLAG_PASS" value=<?=$ANTIPLAG_PASS?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('ANTIPLAG_COMPANY_NAME') ?></td>
                <td><input type="text" name="ANTIPLAG_COMPANY_NAME" id="ANTIPLAG_COMPANY_NAME" value=<?=$ANTIPLAG_COMPANY_NAME?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('ANTIPLAG_APICORP_ADDRESS') ?></td>
                <td><input type="text" name="ANTIPLAG_APICORP_ADDRESS" id="ANTIPLAG_APICORP_ADDRESS" value=<?=$ANTIPLAG_APICORP_ADDRESS?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('Q_FILES_ALL') ?></td>
                <td><input type="text" name="Q_FILES_ALL" id="Q_FILES_ALL" value=<?=$Q_FILES_ALL?>></td>
            </tr>
            <tr>
                <td ><? echo GetMessage('SERVICES_FOR_CHECK') ?></td>
                <td><input type="text" name="SERVICES_FOR_CHECK" id="SERVICES_FOR_CHECK" value=<?=$SERVICES_FOR_CHECK?>></td>
            </tr>

        <? $tabControl->Buttons(); ?>

            <input type="submit" name="Update" <? if ($MOD_RIGHT < "W") echo "disabled" ?> value="<? echo GetMessage("MAIN_SAVE") ?>">
            <input type="reset" name="reset" value="<? echo GetMessage("MAIN_RESET") ?>">
            <input type="hidden" name="Update" value="Y">
            <?= bitrix_sessid_post(); ?>
            <? $tabControl->End(); ?>
        </form>
<?endif;?>