<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Type;

class CAgentsAntyplag
{
    function send_file_to_antiplag(){
        //$hd = fopen(__DIR__ . "/log_order_parce_" . time() . ".txt", "a");
        //fwrite($hd, print_r("sfsdfsdf", 1));
        //fclose($hd);
        if(!CModule::IncludeModule("iblock"))
            return false;
        $ANTIPLAGIAT_URI = COption::GetOptionString("mcart.antiplagiarism", "ADDRESS_ANTIPLAG", "http://testapi.antiplagiat.ru");
        $ADDRESS_CUR_SITE = COption::GetOptionString("mcart.antiplagiarism", "ADDRESS_CUR_SITE", "");

        $Q_FILES_ALL = COption::GetOptionString("mcart.antiplagiarism", "Q_FILES_ALL", "10");
        //$Q_FILES_ALL = trim($Q_FILES_ALL);
        // ������� ������� �������(https)
        $SERVICES_FOR_CHECK = COption::GetOptionString("mcart.antiplagiarism", "SERVICES_FOR_CHECK", "wikipedia");
        $LOGIN = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_LOGIN", "testapi@antiplagiat.ru");
        $PASSWORD = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_PASS", "testapi");
        $COMPANY_NAME = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_COMPANY_NAME", "testapi");
        $APICORP_ADDRESS = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_APICORP_ADDRESS", "api.antiplagiat.ru:44902");
        $client = new SoapClient("https://$APICORP_ADDRESS/apiCorp/$COMPANY_NAME?singleWsdl",
            array("trace"=>1,
                "login"=>$LOGIN,
                "password"=>$PASSWORD,
                "soap_version" => SOAP_1_1,
                "features" => SOAP_SINGLE_ELEMENT_ARRAYS,
            ));



        $res = CIBlock::GetList(Array(), Array("CODE" => 'turn_in_antiplagiarism'), true);
        while($ar_res = $res->Fetch())
        {
            $idIBlockQ = $ar_res['ID'];
        }

        $arFilter = array(
            "IBLOCK_ID" => $idIBlockQ,
            "PROPERTY_CHACK_STATUS" => "New"
        );

        $arSelect = array("ID", "PROPERTY_ID_IBL_LIST", "PROPERTY_ID_FILE_FROM_LIST", "PROPERTY_ID_FILE_FROM_SERVER", "PROPERTY_PATH_FILE_FROM_SERVER", "NAME");

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
        $filesArr = array();
        $cc = 1;
        while($ob = $res->Fetch())
        {
            //echo "<pre>";
            //print_r($ob);
            //echo "</pre>";
            //$info = new SplFileInfo($ob['PROPERTY_PATH_FILE_FROM_SERVER_VALUE']);
            //var_dump($info->getExtension());

            $filesArr[] = $ob;
            $cc++;
            if($Q_FILES_ALL == $cc)
                break;
        }

        if(!empty($filesArr)) {
            foreach($filesArr as $arFile){
                $path_parts = pathinfo($arFile['PROPERTY_PATH_FILE_FROM_SERVER_VALUE']);
                $mystring = $arFile['PROPERTY_PATH_FILE_FROM_SERVER_VALUE'];
                $findme1 = "http://";
                $findme2 = "https://";
                if((strpos($mystring, $findme1) !== false) || (strpos($mystring, $findme2) !== false)){
                    $filecontent = file_get_contents($arFile['PROPERTY_PATH_FILE_FROM_SERVER_VALUE']);
                } else {
                    $filecontent = file_get_contents($ADDRESS_CUR_SITE . $arFile['PROPERTY_PATH_FILE_FROM_SERVER_VALUE']);
                }


                $data = array("Data" => $filecontent,
                    "FileName" => $arFile['NAME'] . "." . $path_parts['extension'],
                    "FileType" => "." . $path_parts['extension']);

                $uploadResult = $client->UploadDocument(array("data" => $data));
                $id = $uploadResult->UploadDocumentResult->Uploaded[0]->Id;
                $idInt = $id->Id;
                $client->CheckDocument(array("docId" => $id, "checkServicesList" => array($COMPANY_NAME, $SERVICES_FOR_CHECK)));

                $PROP['CHACK_STATUS'] = 'InProgress';
                $PROP['ID_IN_ANTIPLOG'] = $idInt;
                $PROP["ID_IBL_LIST"] = $arFile['PROPERTY_ID_IBL_LIST_VALUE'];
                $PROP["ID_FILE_FROM_LIST"] = $arFile['PROPERTY_ID_FILE_FROM_LIST_VALUE'];
                $PROP["ID_FILE_FROM_SERVER"] = $arFile['PROPERTY_ID_FILE_FROM_SERVER_VALUE'];
                $PROP["PATH_FILE_FROM_SERVER"] = $arFile['PROPERTY_PATH_FILE_FROM_SERVER_VALUE'];
                $el = new CIBlockElement;
                $arLoadProductArray = Array(
                    "PROPERTY_VALUES" => $PROP,
                );

                $res = $el->Update($arFile['ID'], $arLoadProductArray);
            }
        }

        return "CAgentsAntyplag::send_file_to_antiplag();";
    }


    function chack_status_file_antiplag(){
        if(!CModule::IncludeModule("iblock"))
            return false;
        $ANTIPLAGIAT_URI = COption::GetOptionString("mcart.antiplagiarism", "ADDRESS_ANTIPLAG", "http://testapi.antiplagiat.ru");
        $ADDRESS_CUR_SITE = COption::GetOptionString("mcart.antiplagiarism", "ADDRESS_CUR_SITE", "");

        $Q_FILES_ALL = COption::GetOptionString("mcart.antiplagiarism", "Q_FILES_ALL", "10");
        //$Q_FILES_ALL = trim($Q_FILES_ALL);
        // ������� ������� �������(https)
        $LOGIN = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_LOGIN", "testapi@antiplagiat.ru");
        $PASSWORD = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_PASS", "testapi");
        $COMPANY_NAME = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_COMPANY_NAME", "testapi");
        $APICORP_ADDRESS = COption::GetOptionString("mcart.antiplagiarism", "ANTIPLAG_APICORP_ADDRESS", "api.antiplagiat.ru:44902");
        $client = new SoapClient("https://$APICORP_ADDRESS/apiCorp/$COMPANY_NAME?singleWsdl",
            array("trace"=>1,
                "login"=>$LOGIN,
                "password"=>$PASSWORD,
                "soap_version" => SOAP_1_1,
                "features" => SOAP_SINGLE_ELEMENT_ARRAYS,
            ));



        $res = CIBlock::GetList(Array(), Array("CODE" => 'turn_in_antiplagiarism'), true);
        while($ar_res = $res->Fetch())
        {
            $idIBlockQ = $ar_res['ID'];
        }

        $arFilter = array(
            "IBLOCK_ID" => $idIBlockQ,
            "PROPERTY_CHACK_STATUS" => 'InProgress'
        );

        $arSelect = array("ID", "PROPERTY_ID_IBL_LIST", "PROPERTY_ID_FILE_FROM_LIST", "PROPERTY_ID_FILE_FROM_SERVER", "PROPERTY_PATH_FILE_FROM_SERVER", "PROPERTY_ID_IBL_LIST_VALUE", "PROPERTY_ID_IN_ANTIPLOG", "NAME");

        $res = CIBlockElement::GetList(Array(), $arFilter, false, Array("nPageSize"=>50), $arSelect);
        $filesArr = array();
        $cc = 1;
        while($ob = $res->Fetch())
        {
            //echo "<pre>";
            //print_r($ob);
            //echo "</pre>";
            //$info = new SplFileInfo($ob['PROPERTY_PATH_FILE_FROM_SERVER_VALUE']);
            //var_dump($info->getExtension());
            $filesArr[] = $ob;
            $cc++;
            if($Q_FILES_ALL == $cc)
                break;
        }
//echo "<pre>";
//print_r($filesArr);
//echo "</pre>";
        if(!empty($filesArr)) {
            foreach($filesArr as $arFile){
                if(!empty($arFile['PROPERTY_ID_IN_ANTIPLOG_VALUE'])){
                    //echo $arFile['PROPERTY_ID_IN_ANTIPLOG_VALUE'];
                    $d = new stdClass();
                    $d->Id = $arFile['PROPERTY_ID_IN_ANTIPLOG_VALUE'];
                    $d->External = NULL;

                    $status = $client->GetCheckStatus(array("docId" => $d));
                    $st = $status->GetCheckStatusResult->Status;
                    //echo $st;
                    if($st == "Ready"){
                        $report = $client->GetReportView(array("docId" => $d));
                        $res_plag = $report->GetReportViewResult->Summary->DetailedScore->Plagiarism;
                        //echo "<pre>";
                        //var_dump($report);
                        //echo "</pre>";
                        /*foreach ($report->GetReportViewResult->CheckServiceResults as $checkService) {
                            if (isset($checkService->Sources)) {
                                foreach($checkService->Sources as $source) {
                                    $proc = $source->ScoreByReport;
                                    //echo $proc;
                                }
                            } else {
                                $proc = 0;
                            }
                        }*/
                        if(!empty($res_plag)){
                            $proc = $res_plag;
                        }else{
                            $proc = 0;
                        }



                        $PROP['CHACK_STATUS'] = $st;
                        $PROP['ID_IN_ANTIPLOG'] = $arFile['PROPERTY_ID_IN_ANTIPLOG_VALUE'];
                        $PROP["ID_IBL_LIST"] = $arFile['PROPERTY_ID_IBL_LIST_VALUE'];
                        $PROP["ID_FILE_FROM_LIST"] = $arFile['PROPERTY_ID_FILE_FROM_LIST_VALUE'];
                        $PROP["ID_FILE_FROM_SERVER"] = $arFile['PROPERTY_ID_FILE_FROM_SERVER_VALUE'];
                        $PROP["PATH_FILE_FROM_SERVER"] = $arFile['PROPERTY_PATH_FILE_FROM_SERVER_VALUE'];
                        $PROP["PROC_PLAG"] = $proc;
                        $el = new CIBlockElement;
                        $arLoadProductArray = Array(
                            "PROPERTY_VALUES" => $PROP,
                        );

                        $res = $el->Update($arFile['ID'], $arLoadProductArray);
                    }
                }
            }
        }

        return "CAgentsAntyplag::chack_status_file_antiplag();";
    }
}